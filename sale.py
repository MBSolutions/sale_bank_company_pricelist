#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import Pool


class Sale(ModelSQL, ModelView):
    _name = 'sale.sale'

    def create_invoice(self, cursor, user, sale_id, context=None):
        '''
        Create an invoice for the sale

        :param cursor: the database cursor
        :param user: the user id
        :param sale_id: the sale id
        :param context: the context

        :return: the created invoice id or None
        '''
        pool = Pool()
        invoice_obj = pool.get('account.invoice')
        journal_obj = pool.get('account.journal')
        invoice_line_obj = pool.get('account.invoice.line')
        sale_line_obj = pool.get('sale.line')

        if context is None:
            context = {}

        sale = self.browse(cursor, user, sale_id, context=context)

        invoice_lines = self._get_invoice_line_sale_line(cursor, user, sale,
                context=context)
        if not invoice_lines:
            return

        journal_id = journal_obj.search(cursor, user, [
            ('type', '=', 'revenue'),
            ], limit=1, context=context)
        if journal_id:
            journal_id = journal_id[0]

        ctx = context.copy()
        ctx['user'] = user
        invoice_id = invoice_obj.create(cursor, 0, {
            'company': sale.company.id,
            'type': 'out_invoice',
            'reference': sale.reference,
            'journal': journal_id,
            'party': sale.party.id,
            'invoice_address': sale.invoice_address.id,
            'currency': sale.currency.id,
            'account': sale.party.account_receivable.id,
            'payment_term': sale.payment_term.id,
            'bank_account': sale.bank_account.id,
            'pricelist': sale.pricelist.id,
        }, context=ctx)

        for line_id in invoice_lines:
            for vals in invoice_lines[line_id]:
                vals['invoice'] = invoice_id
                invoice_line_id = invoice_line_obj.create(cursor, 0, vals,
                        context=ctx)
                sale_line_obj.write(cursor, user, line_id, {
                    'invoice_lines': [('add', invoice_line_id)],
                    }, context=context)

        invoice_obj.update_taxes(cursor, 0, [invoice_id], context=ctx)

        self.write(cursor, user, sale_id, {
            'invoices': [('add', invoice_id)],
        }, context=context)
        return invoice_id

Sale()
